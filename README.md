# VIII. Notifications, pop-up et syndrome de surcharge cognitive

## La surcharge cognitive, un phénomène accentué par le numérique 

Un des facteurs importants de la numérisation globale a été l’adoption croissante et rapide des outils numériques au sein des lieux de travail, places centrales des sociétés contemporaines.

La division du travail, concept important dans cette organisation, et permettant une optimisation des tâches par la spécialisation des individus, a en conséquence été profondément modifiée.

En effet, d'après Lahlou, « le travail, quel que soit son objet, a tendance à être transféré des humains vers des automates spécialisés (machines), dont le rendement pour une tâche répétitive particulière est meilleur que celui d’un humain non spécialisé. » [1], p.2.
À la suite d’une première vague d’automatisation de l’industrie, orientant une majorité des travailleurs du secteur secondaire vers le tertiaire, le développement et l’adoption des outils numériques dans le monde des services ont provoqué un phénomène proche pour le travail de l’Information. Une majorité des traitements autrefois effectués par des humains (chercheurs, comptables, informaticiens) peut maintenant être gérée de manière automatique par des calculs. Ces changements entrainent une prolifération des données et une répartition globalisée des traitements entre automates. Pour les acteurs humains à la supervision de ces processus, « la nature même des enjeux économiques en est modifiée : c’est désormais pour des « parts d’attention » autant que pour des « parts de marché » que se battront les opérateurs (François Peccoud, communication personnelle, 1998). » [1], p.3.
Comme présenté lors du chapitre V de ce mémoire, dans un monde surinformé, l’attention humaine devient la nouvelle ressource à obtenir. Émerge progressivement un phénomène qui, s’il était connu avant l’avènement du numérique, atteint désormais des proportions systémiques : le syndrome de la saturation cognitive, ou *Cognitive Overflow Syndrom*, abrégé COS.

Avec l’accroissement des informations et l’injonction permanente à leur traitement, « de nombreux travailleurs intellectuels expriment un stress et une frustration que l’on pourrait résumer par “je passe mon temps à régler des détails et je n’arrive pas à réaliser mon vrai travail ”, “ l’urgent passe avant l’important ”, “ je suis débordé ”. » [1] p.4. Un stress s’installe chez les individus qui se sentent noyés par la quantité de données à traiter. Ils perdent le sens de leur travail et ne sont plus en mesure d’identifier des causes précises à chacun des symptômes ressentis.

Si les recherches effectuées par Saadi Lahlou, dans une publication de l’année 2000 [1], analysaient déjà ce phénomène sur les lieux de travail, celui-ci s’est désormais étendu à la sphère privée, notamment par le biais de l’adoption massive des smartphones.
Lors de l’usage d'appareils numériques, des évènements comme les pop-up ou les notifications attirent volontairement l’attention de l’utilisateur, qui détourné de sa tâche, ne serait-ce que quelques millièmes de seconde, ressent l'impression progressive de "se perdre" sur la toile.

L'objectif du chapitre de cette semaine sera de s'attarder sur les liens entre numérique et saturation cognitive. Dans un premier temps, nous reviendrons sur le COS en espace de travail numérisé avant d'évoquer les mécanismes de détournement par l'intermédiaire des publicités web. Enfin, nous étudierons les *dark patterns*, des interfaces utilisateurs manipulatrices exploitant la diminution d'attention liée à la surcharge cognitive. 

[1] – Lahlou, S. [*Travail de bureau et débordement cognitif*](https://www.researchgate.net/profile/Saadi_Lahlou/publication/44219183_Travail_de_bureau_et_debordement_cognitif/links/577b7fc808ae213761ca8472/Travail-de-bureau-et-debordement-cognitif.pdf). XXXVème Congrès de la SELF. Toulouse, 21//9/2000.

## Le syndrome de surcharge cognitive dans un environnement de travail digitalisé

Comme mentionné dans la partie introductive de ce chapitre, le syndrome de surcharge cognitive (*COS*) touche de plus en plus de personnes, notamment à notre époque où le numérique est très présent. Dans cette partie, nous nous intéresserons au *COS* dans un environnement de travail digitalisé.

Une étude de la division R&D d'EDF **[1]** a mené un programme de recherches pour notamment lutter contre la surcharge cognitive et pour permettre aux opérateurs de travailler dans de bonnes conditions tout en utilisant au mieux les nouvelles technologies (NTIC). En effet, qui n'a jamais dépilé ses mails alors qu'il devait réaliser une tâche importante à son travail.

La plupart des travailleurs expriment un sentiment de stress ainsi de frustration et lors d'une étude menée au sein d'EDF, plus de **50%** admettent être débordés, **68%** ont le sentiment de "faire passer l'urgent avant l'important" et **61.5%** ne parviennent pas à faire ce qu'ils avaient prévu de faire dans une journée. Cela est notamment dû à une perte de contrôle dans leur activité au quotidien, pouvant être liée à une surcharge cognitive.

Dans l'étude, ils ont demandé à certains de leurs employés de porter une caméra "subjective" pouvant filmer et enregistrer le son, qu'ils devaient porter au niveau des yeux afin d'analyser au mieux les journées de ces employés. Dans une de leur expérience, Paul, responsable d'un département dans un grand centre de recherche, est filmé. Analysons ensemble ses actions **[1]** :

> **{14h : 31’ : 35’’}** Paul rentre dans son bureau. Soupire. Ferme la porte, et en même temps balaie du regard son bureau (entièrement couvert de piles de documents).

> **{14 : 31 : 49}** (toujours debout) Saisit un document D1 [article photocopié] au sommet de la pile A située au centre du bureau, devant sa chaise. Va poser D1 sur sa table de réunion, prend une pile B contenant d’autres documents analogues et y range D1.

> **{14 : 32 : 00}** En reposant la pile B, prend un document D2 [liste d’adresses] posé sur la table juste à côté de la pile B. Plie le document. Soupire. D2 à la main, retourne vers son bureau, balaie celui-ci du regard.

> **{14 : 32 : 11}** Allume son ordinateur.

> **{14 : 32 : 12}** Range D2 dans une pile C à gauche de son écran.

> **{14 : 32 : 18}** S’assied. Tire son clavier. Tape son mot de passe.

> **{14 : 32 : 28}** Soupire. Balaie son bureau du regard. (assis) Fait rouler sa chaise jusqu’à la table de réunion, saisit (main droite) un bloc de papier et un crayon, avec un document D3 [feuille A4] dessus. Roule à son bureau et pose D3 sur une pile D sur son bureau. Saisit son agenda sur son bureau avec sa main gauche (etc.).

> **{14 : 32 : 28}** - **{14 : 36 : 15}** Paul continue de dégager progressivement un espace libre devant lui, légèrement à gauche du clavier. Il le dégage en “ se débarrassant ” successivement des artefacts qui occupent cet espace, soit en les rangeant dans son bureau, soit en les buvant (canette de Perrier), soit en les traitant (lecture, annotation), et, à un moment, en allant en donner un à sa secrétaire.

> **{14 : 36 : 17}** Paul sort de sa serviette un cahier contenant des documents, dont un compte-rendu d’une réunion à laquelle il a participé, rédigé par une des ses collaboratrices, qu’il veut compléter et modifier. Il dispose sur l’espace dégagé ses notes manuscrites (cahier), et une version papier déjà annotée du compte-rendu. Le temps d’ouvrir sur son ordinateur la version numérique du document (avec un détour par l’ouverture de son e-mail), de régler un problème mécanique concernant la “ souris ” de son ordinateur, de brancher sa messagerie vocale pour ne pas être dérangé, de transférer la pièce jointe sur disque depuis le logiciel de messagerie et de l’ouvrir avec le traitement de texte,

> **{14 : 40 : 40}** Paul commence à taper ses corrections, en s’aidant de la version papier et de ses notes disposées sur son bureau.

> **{14 : 41 : 34}** Paul est interrompu dans cette tâche par un visiteur. Paul ne reprend que quelques minutes plus tard (après avoir d’ailleurs dû dégager une nouvelle fois son espace de travail d’un annuaire utilisé lors de l’interruption). Il terminera la correction du document 8 mn après la reprise, puis commencera à rédiger un e-mail d’accompagnement du compte-rendu. Il sera interrompu dans cette tâche 1mn 42 secondes plus tard par un autre visiteur, pendant 1mn 29 s ; puis la terminera dans la minute suivante en envoyant le e-mail avec la pièce jointe corrigée.

Au final, Paul aura effectué sa tâche en onze minutes en temps cumulé, dont neuf minutes pour préparer son espace de travail, sachant qu'il a été interrompu deux fois au cours de cette tâche.

De manière plus imagée, supposons qu'une personne ait pour tâche de réaliser des figures qui rapportent plus ou moins de points (par exemple : un rond rouge : un point, un carré bleu : cinquante points, la figure complexe avec un carré et quatre flèches : cent points). Il se trouve devant l’assemblage suivant. Que fait-il en premier ?

![](https://md.picasoft.net/uploads/upload_07cafc5189069c278d2af0ed9defca28.png)

La plupart des gens choisiront de commencer par le rond rouge afin de libérer de l'espace pour pouvoir s'attaquer aux problèmes plus compliqués. Ce raisonnement devient problématique lorsque le nombre de ronds rouges est important. Malheureusement de nombreuses personnes procèdent ainsi : les employés vont souvent préférer répondre à leurs mails avant de commencer une tâche relativement lourde. De manière similaire sur le web, les notifications ainsi que les pop-up vont souvent détourner l'attention de l'utilisateur, ce que nous aborderons dans la partie suivante. 

[1] – Lahlou, S. [*Travail de bureau et débordement cognitif*](https://www.researchgate.net/profile/Saadi_Lahlou/publication/44219183_Travail_de_bureau_et_debordement_cognitif/links/577b7fc808ae213761ca8472/Travail-de-bureau-et-debordement-cognitif.pdf). XXXVème Congrès de la SELF. Toulouse, 21//9/2000.


## Effet des pop-up et des animations sur l'attention

La croissance du Web s'est accompagnée d'une concurrence plus dure entre ses différents acteurs pour attirer l'attention des internautes. Les agences de publicité cherchent alors à attirer les visiteurs par un ensemble de méthodes visuelles (notamment avec une combinaison entre les polices d'écritures, les couleurs et les mouvements) et auditives (de la musique de fond qui se démarre automatiquement). L'étude **Orienting Response and Memory for Web Advertisements** [1] cherche alors à quantifier l'efficacité des pop-up et des notifications en tant que stimulus instantanés et leurs effets sur la mémoire.

Dans un premier temps, il est important de définir les différents types de publicités qui se baladent sur les sites Web:

- L'espèce la plus courante est la **bannière** qui présente son contenu de manière statique placée dans les zones de grande visilibité et qui s'étend normalement sur l'écran entier de l'utilisateur (les bannières peuvent être des vidéos, des images ou du texte)
- Les **pop-ups**, une variante plus invasive, sont dynamiques et apparaissent après que l'ensemble du contenu du site a été affiché sur l'écran de l'utilisateur.

L'hypothèse dominante dans le domaine de la publicité est que les pop-ups sont beaucoup plus efficaces que les bannières puisque son apparition soudaine provoque une rupture visuelle qui devient un stimulus nécessitant de l'attention. Cette mobilisation de l'attention pour réagir à un changement soudain de l'environnement est intitulée la **réaction d'orientation** (ou orienting response en anglais) et peut engendrer une baisse temporaire du rythme cardiaque. Les théories sur l'*effet des mouvements* viennent soutenir cette hypothèse. Selon elles, l'homme, au cours de son évolution, a développé une prédisposition pour fixer son attention sur des objets en mouvement; qui peut être un danger (e.g. un prédateur) ou une opportunité (e.g. une proie ou un ruisseau). De ce fait, les publicités dynamiques attirent forcément l'attention de l'internaute.

Afin de prouver ce phénomène de manière empirique, les chercheurs ont observé le rythme cardiaque de soixante personnes qui naviguent quatre sites différents avec des publicités différentes:
- *AA*: bannière et pop-up dynamique
- *AS*: bannière dynamique et pop-up statique
- *SA*: bannière statique et pop-up statique
- *SS*: bannière et pop-up statique 

*Changement du rythme cardiaque (BPM) en fonction du type de publicité et du temps depuis la visualisation*
![BPM as function of ad type and time](https://md.picasoft.net/uploads/upload_125d9f99326ae3faa48b457391e565df.png)

*Changement du rythme cardiaque (BPM) en fonction de la présence d'animation et du temps depuis la visualisation pour les pop-ups*
![BPM as function of static and time](https://md.picasoft.net/uploads/upload_b5f2a5866935c92496175e5fab5a6546.png)

La réaction d'orientation se produit bien lors de la visualisation des pop-ups et pas avec les bannières. De plus, pour les pop-ups les animations semblent accentuer la réponse. Cette étude vient donc confirmer l'efficacité des pop-ups sur la captation d'attention des internautes. 

Tout de même, ces types de publicités impactent fortement la vitesse et la facilité de navigation. L'efficacité de leur usage par des sites malveillants (notamment de fraude de support informatique) a aussi contribué au déclin de l'image des pop-ups. C'est pourquoi, actuellement, les grands navigateurs (Chrome, Firefox) les bloquent tous par défaut. Tout de même, cette étude révèle que les mécanismes cognitifs peuvent facilement être exploités par des agences de publicité pour augmenter leur taux de clics.

*pop-up frauduleux pour pousser l'utilisateur à contacter un support technique. Ces stratégies affectent de manière disproportionnée les personnes agées [2]*
![fraude](https://md.picasoft.net/uploads/upload_721635fc6cce33c73d4e2a2d866209c6.jpg)


[1] - Diao F, Sundar SS. [Orienting Response and Memory for Web Advertisements:: Exploring Effects of Pop-Up Window and Animation](https://journals.sagepub.com/doi/10.1177/0093650204267932). Communication Research. 2004;31(5):537-567. doi:10.1177/0093650204267932

[2] - [Older adults hardest hit by tech support scams](https://www.ftc.gov/news-events/blogs/data-spotlight/2019/03/older-adults-hardest-hit-tech-support-scams), Emma Fletcher, consulté le 29/11/2020

## Les *Dark Patterns*, le syndrome de surcharge cognitive exploité commercialement

Le terme *dark patterns*  a été inventé par Harry Brignull en aout 2010. Il se réfère à des interfaces utilisateurs conçues pour manipuler un utilisateur, en orientant ses choix pour avantager le site sur lequel il navigue. Ces pratiques ne sont en général pas illégales en soi : Un utilisateur vigilant pourra toujours faire le choix qu'il veut, rien n'est forcé. Les *dark patterns* les plus communs consistent par exemple à jouer sur la couleur et la taille des boutons par exemple en présentant l'option "S'inscrire à la newsletter" de façon bien plus visible que "Ne pas s'inscrire".

Les *patterns* qui nous intéressent dans ce chapitre sont ceux ayant un lien avec le syndrome de surcharge cognitive. Comme nous l'avons vu précédemment, naviguer sur le web nous expose à une multitude de notifications, de *pop-ups*, et d'informations en général. Cette abondance de distractions fait prendre de nombreuses habitudes aux utilisateurs non avertis. Ce sont ces habitudes qui peuvent ensuite être exploitées dans des interfaces malicieuses. 

Une de ces habitudes est de naviguer très rapidement de pages en pages : L'utilisateur a pris l'habitude de voir de nombreux liens sur les pages web qu'il visite, le redirigeant sur d'autres pages. Il est alors possible d'exploiter ce comportement. Par exemple, un site de vente en ligne peut proposer une offre très intéressante à un utilisateur. Une fois qu'il a cliqué sur cette offre, elle apparaitra alors comme indisponible (par exemple pour cause de rupture de stock), mais d'autres offres seront proposées. Ces offres seront moins intéressantes, mais un grand nombre d'utilisateurs ayant tellement l'habitude de naviguer de lien en lien, ils ne reviendront pas en arrière sur leur recherche initiale, mais continueront de lien en lien.
Sous les noms de "bait and switch" cette pratique peut avoir plusieurs variantes, mais le principe reste le même : Faire cliquer l'utilisateur sur un lien pour une raison quelconque, pour ensuite le rediriger ou lui faire faire certaines actions non prévues à la base.
Pour se prémunir de cette pratique, une seule solution : Rester concentrer sur ses recherches initiales, et ne pas se laisser déconcentrer par des distractions apparues plus tard. Cependant, cela n'est possible qu'en ayant conscience de cette problématique.

Un autre point est important à considérer. Une autre habitude que prennent certains utilisateurs des sites web modernes est de tout simplement ignorer une grande quantité d'information. Cette habitude a bien toujours pour origine la surcharge cognitive décrite précédemment : en la présence d'une trop grande quantité de signaux, tous ne sont pas traités. Mais un site web bien construit peut alors exploiter cette habitude, en plaçant des informations légalement nécessaires à des emplacements qui ne seront pas ou peu lus par les utilisateurs.

Voici un exemple d'une telle méthode : 

![](https://darkpatterns.org/images/5cc0caee5250c969d98e1499_compressed_58a5cb8c8d981_screenshot-2013-10-25-09-47-46.png.jpeg)

Sur ce site, de réservation de vols Australien, choisir sa place est payant. Cependant, choisir une place aléatoire est gratuit. Lorsque l'utilisateur arrive sur cette page, cette information est bien communiquée, et une place est pré-sélectionnée. Cependant, ce que l'utilisateur peut ne pas remarquer est que ce choix fait par le site qui semble aléatoire n'est pas le choix gratuit : En passant à la suite, il paiera le supplément. Pour ne rien payer, il devra trouver le petit lien "ne pas choisir de siège". Plus de détails sur cette situation sont disponibles sur https://darkpatterns.org/types-of-dark-pattern/misdirection.html.

Dans ce cas, contrairement a précédemment, il faut alors porter une attention particulière aux détails des sites web, pour ne pas tomber dans un piège.

Ces deux *dark patterns* très génériques mettent en évidence un problème concret : Dans certain cas, porter son attention sur des éléments a priori auxiliaires d'une page peut inciter l'utilisateur à effectuer des actions non prévues à la base, mais dans d'autres cas cette même attention peut lui permettre de repérer des informations importantes. La seule solution est alors de s'éduquer aux problématiques liées à la surcharge cognitive pour entrainer son attention à se porter sur les éléments réellement importants.

[1] - [Types of dark pattern](https://darkpatterns.org/types-of-dark-pattern.html), consulté le 29/11/2020

[2] - [The Year Dark Patterns Won](https://www.fastcompany.com/3066586/the-year-dark-patterns-won), consulté le 29/11/2020

[3] - [When Websites Won’t Take No for an Answer](https://www.nytimes.com/2016/05/15/technology/personaltech/when-websites-wont-take-no-for-an-answer.html?_r=1), consulté le 29/11/2020
